//
//  main.m
//  tkt
//
//  Created by Sergey Voronov on 3/19/18.
//  Copyright © 2018 Sergey Voronov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TKTAppDelegate class]));
    }
}
