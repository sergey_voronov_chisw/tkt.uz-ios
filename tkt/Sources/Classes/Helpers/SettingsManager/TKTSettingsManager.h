//
//  TKTSettingsManager.h
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    kTKTHttpRequestKey_Unknown = -1,
    
    kTKTHttpRequestKey_SsoToken,
    kTKTHttpRequestKey_SsoRefreshToken,
    
    kTKTHttpRequestKey_Count
}   TKTHttpRequestKey;

@class TKTHttpRequestData;

@interface TKTSettingsManager : NSObject

TKT_SHARED_INSTANCE_DECLARATION

- (void)setupHttpRequests;
- (TKTHttpRequestData *)httpRequestDataAtKey:(TKTHttpRequestKey)key;

- (NSString *)baseUrl;

@end
