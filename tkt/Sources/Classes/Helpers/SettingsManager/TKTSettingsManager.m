//
//  TKTSettingsManager.m
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import "TKTSettingsManager.h"
#import "TKTHttpRequestData.h"
#import "NSDictionary+Extension.h"

static NSString *const kTKTHttpRequestApiEndpointsKeypath       = @"API.Endpoints";

static NSString *const kTKTHttpRequestKeyString_SsoToken        = @"sso.connect.Token";
static NSString *const kTKTHttpRequestKeyString_SsoRefreshToken = @"sso.connect.RefreshToken";

@interface TKTSettingsManager ()

@property (nonatomic, strong) NSDictionary *settings;
@property (nonatomic) NSMutableDictionary *httpRequests;

@end

@implementation TKTSettingsManager

TKT_SHARED_INSTANCE_IMPLEMENTATION

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        NSString *settingsFilePath =
            [[NSBundle bundleForClass:[self class]] pathForResource:@"TKTSettings"
                                                             ofType:@"plist"];
        
        NSData *data = [NSData dataWithContentsOfFile:settingsFilePath];
        
        NSPropertyListFormat format;
        NSError *error;
        NSDictionary* allSettings =
            [NSPropertyListSerialization propertyListWithData:data
                                                      options:NSPropertyListImmutable
                                                       format:&format
                                                        error:&error];
        
        NSDictionary *currentSettings = [allSettings dictionaryForKey:[self currentEnvironment]];
        NSAssert(currentSettings != nil, @"Unable to load settings");
        
        self.settings = currentSettings;
        
        [self setupHttpRequests];
    }
    return self;
}

- (void)setupHttpRequests {
    
    self.httpRequests = [[NSMutableDictionary alloc] initWithCapacity:kTKTHttpRequestKey_Count];
    
    for (TKTHttpRequestKey key = kTKTHttpRequestKey_Unknown + 1; key < kTKTHttpRequestKey_Count; ++key) {
        
        @autoreleasepool {
            
            TKTHttpRequestData *httpRequestData = [TKTHttpRequestData new];
            httpRequestData.endpoint = [self apiParameter:@"Endpoint" forHttpRequestKey:key];
            httpRequestData.method   = [self apiParameter:@"Method"   forHttpRequestKey:key];
            [self.httpRequests setObject:httpRequestData forKey:@(key)];
        }
    }
}

- (TKTHttpRequestData *)httpRequestDataAtKey:(TKTHttpRequestKey)key {
    
    TKTHttpRequestData *result;
    
    if (key > kTKTHttpRequestKey_Unknown || key < kTKTHttpRequestKey_Count) {
        
        result = [self.httpRequests objectForKey:@(key)];
    }
    
    return result;
}

#pragma mark - Private methods

- (NSString *)apiParameter:(NSString *)parameter forHttpRequestKey:(TKTHttpRequestKey)key  {
    
    NSString *keyPath = [NSString stringWithFormat:@"%@.%@.%@",
                         kTKTHttpRequestApiEndpointsKeypath,
                         [[self class] httpRequestKeyStringFromKey:key],
                         parameter];
    return [self.settings valueForKeyPath:keyPath];
}

- (NSString *)apiUrlStringForHttpRequestKey:(TKTHttpRequestKey)key {
    
    NSString *httpRequestKeyString = [[self class] httpRequestKeyStringFromKey:key];
    return [self stringForHttpRequestKeyString:httpRequestKeyString];
}

+ (NSString *)httpRequestKeyStringFromKey:(TKTHttpRequestKey)key {
    
    NSString *result;
    
    switch (key) {
        case kTKTHttpRequestKey_SsoToken       : result = kTKTHttpRequestKeyString_SsoToken       ; break;
        case kTKTHttpRequestKey_SsoRefreshToken: result = kTKTHttpRequestKeyString_SsoRefreshToken; break;

        default: break;
    }
    
    return result;
}

- (NSString *)currentEnvironment {
    
    return CURRENT_ENVIRONMENT;
}

//- (NSString *)stringForKey:(NSString *)key {
//    
//    return [self.settings stringForKey:key];
//}
//
//- (BOOL)boolForKey:(NSString *)key {
//    
//    return [self.settings boolForKey:key];
//}

- (NSString *)stringForHttpRequestKeyString:(NSString *)httpRequestKeyString {
    
    NSString *result;
    
    NSRange range = [httpRequestKeyString rangeOfString:@"."];
    if (range.location != NSNotFound) {
        
        NSString *category = [httpRequestKeyString substringToIndex:range.location];
        NSString *key      = [httpRequestKeyString substringFromIndex:range.location + 1];
        result = [self stringForKey:key category:category];
    }
    
    return result;
}

- (NSString *)stringForKey:(NSString *)key category:(NSString *)category {
    
    NSString *keyPath = [NSString stringWithFormat:@"%@.%@%@%@",
                         kTKTHttpRequestApiEndpointsKeypath,
                         category,
                         category.length ? @"." : @"",
                         key];
    return [NSString stringWithFormat:@"%@%@%@",
            category,
            category.length ? @"/" : @"",
            [self.settings valueForKeyPath:keyPath]
            ];
}

- (NSString *)baseUrl {
    
    return [self.settings valueForKeyPath:@"Base URL"];
}

@end
