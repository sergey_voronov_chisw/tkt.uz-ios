//
//  TKTAppDelegate.h
//  tkt
//
//  Created by Sergey Voronov on 3/19/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

