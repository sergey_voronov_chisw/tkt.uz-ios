//
//  TKTHttpRequestData.m
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import "TKTHttpRequestData.h"

@implementation TKTHttpRequestData

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        _method = @"GET";
    }
    return self;
}

#pragma mark -

- (NSString *)description {
    
    return [NSString stringWithFormat:
            @"\n========== HTTP Request Data =========="
            "\nEndPoint: %@"
            "\nMethod  : %@",
            _endpoint,
            _method
            ];
}

@end
