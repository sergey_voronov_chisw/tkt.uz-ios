//
//  TKTCoreDataManager.m
//  tkt
//
//  Created by Sergey Voronov on 3/19/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import "TKTCoreDataManager.h"

#import <CoreData/CoreData.h>

#define TKTDatabaseName @"tkt"
#define TKTThreadMocKey @"MOC"

@implementation TKTCoreDataManager

TKT_SHARED_INSTANCE_IMPLEMENTATION

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        
        if (_persistentContainer == nil) {
            
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:TKTDatabaseName];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

+ (void)save:(NSError **)error {
    
    [self saveContext:[self managedObjectContext] error:error];
}

+ (void)saveWithCompletion:(void (^)(NSError *))completion {
    
    [self saveContext:[self managedObjectContext] withCompletion:completion];
}

+ (void)cleanDatabase {
    
    [[TKTCoreDataManager sharedInstance] releasePersistentStoreContainer];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? paths[0] : nil;
    NSURL *storeUrl = [NSURL fileURLWithPath:[basePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", TKTDatabaseName]]];
    
    void(^eraseDatabase)(void) = ^(void) {
        DLog(@"DB file will bee removed.");
        
        __autoreleasing NSError *error;
        [[NSFileManager defaultManager] removeItemAtURL:storeUrl error:&error];
        if (error) {
            
            //NSLog(@"\n\n\n Fail to remove db store file %@ \n\n\n", [error localizedDescription]);
        } else {
            
            //NSLog(@"\n\n\n db store file removed!\n\n\n");
        }
    };
    
    eraseDatabase();
}

- (void)releasePersistentStoreContainer {
    
    @synchronized(self) {
        
        _persistentContainer = nil;
    }
}

#pragma mark General Objects Methods

+ (id)newObjectOfClass:(Class)class {
    
    return (id)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(class)
                                             inManagedObjectContext:[self managedObjectContext]];
}

+ (void)deleteObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context {
    
    if (!object) {
        
        //NSLog(@"attemp to delete nil object from context %@ ", context);
        return;
    }
    
    if (!context) {
        
        //NSLog(@"attemp to delete object from nil context %@ ", object);
        return;
    }
    
    if ([object managedObjectContext] != context) {
        
        NSLog(@"attemp to delete object from other context %@ ", object);
        return;
    }
    
    //NSLog(@"Will delete object from context %@", object);
    
    [context deleteObject:object];
}

+ (void)deleteObjectsOfClass:(Class)class {
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(class)];
    NSBatchDeleteRequest *deleteRequest = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
    
    NSError *error;
    [[self managedObjectContext] executeRequest:deleteRequest error:&error];
    if (!error) {
        
        [self save:&error];
    }
}

+ (NSManagedObject *)existingObjectWithObjectID:(NSManagedObjectID *)managedObjectID
                                      inContext:(NSManagedObjectContext *)context {
    
    if (!managedObjectID) {
        
        NSAssert3(NO, @"%@ - %@ : %@", NSStringFromSelector(_cmd), NSStringFromClass([self class]), @"objectID can't be nil");
    }
    
    NSError *error;
    
    NSManagedObject *managedObject = [context existingObjectWithID:managedObjectID error:&error];
    
    if (error) {
        
        // NSAssert2(NO,
        // @"Error while getting existing object by objectID [%@] from data base. Error",
        // managedObjectID,
        // [error localizedDescription]);
        return nil;
    }
    
    return managedObject;
}

+ (NSUInteger)countOfObjectsWithClass:(Class)class andPredicate:(NSPredicate *)predicate {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(class)];
    request.predicate = predicate;
    
    __autoreleasing NSError *error;
    NSUInteger result = [[self managedObjectContext] countForFetchRequest:request error:&error];
    
    if (error) {
        
        //NSLog(@"error %@", error);
    }
    
    return result;
}

+ (NSArray *)objectsWithClass:(Class)class andPredicate:(NSPredicate *)predicate {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(class)];
    request.predicate = predicate;

    __autoreleasing NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
    if (error) {
        
        //NSLog(@"error %@", error);
    }
    
    if (results.count >= 2) {
        
        //NSLog(@"result %@", results);
        // NSAssert1((results.count < 2), @"Database contains more then 1 rpn with ChatId", chatId);
    }
    
    return results;
}

+ (id)calcValue:(NSString *)valueName
       forField:(NSString *)fieldName
        ofClass:(Class)class
   andPredicate:(NSPredicate *)predicate {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(class)];
    request.predicate = predicate;
    request.resultType = NSDictionaryResultType;
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:fieldName];
    NSExpression *functionExpression = [NSExpression expressionForFunction:valueName arguments:@[keyPathExpression]];
    
    NSExpressionDescription *expressionDescription = [NSExpressionDescription new];
    expressionDescription.name = @"Value";
    expressionDescription.expression = functionExpression;
    expressionDescription.expressionResultType = NSInteger32AttributeType;
    
    request.propertiesToFetch = @[expressionDescription];
    
    id result;
    
    NSError *error;
    NSArray *values = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (error) {
        
        //NSLog(@"error %@", error);
    } else if (values.count) {
        
        result = [values.firstObject valueForKey:expressionDescription.name];
    }
    
    return result;
}

#pragma mark General Context Methods

+ (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *result;
    
    NSPersistentContainer *persistentContainer = [[self sharedInstance] persistentContainer];
    
    NSThread *thisThread = [NSThread currentThread];
    if ([thisThread isMainThread]) {
        
        result = persistentContainer.viewContext;
    } else {
        
        result = [[thisThread threadDictionary] objectForKey:TKTThreadMocKey];
        if (!result) {
            
            result = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            result.persistentStoreCoordinator = persistentContainer.persistentStoreCoordinator;
            [[thisThread threadDictionary] setValue:result forKey:TKTThreadMocKey];
        }
    }
    
    return result;
}

#pragma mark General Context Save Methods

+ (void)saveContext:(NSManagedObjectContext *)context error:(NSError **)error {
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator =
        [[[TKTCoreDataManager sharedInstance] persistentContainer] persistentStoreCoordinator];
    
    if (!persistentStoreCoordinator) {
        
        return;
    }
    
    NSManagedObjectContext * __strong strongContext = context;
    
    [strongContext performBlock: ^{
        
        NSError *saveError;
        if ([strongContext save:&saveError]) {
            
            NSManagedObjectContext *parentContext = strongContext.parentContext;
            if (parentContext) {
                
                [parentContext performBlock:^{
                    
                    NSError *parentSaveError;
                    [self saveContext:parentContext error:&parentSaveError];
                    if (parentSaveError) {
                        
                        //NSLog(@"parent save error %@", parentSaveError);
                    }
                }];
            }
        }
    }];
}

+ (void)saveContext:(NSManagedObjectContext *)context withCompletion:(void (^)(NSError *))completion {
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator =
        [[[TKTCoreDataManager sharedInstance] persistentContainer] persistentStoreCoordinator];
    
    if (!persistentStoreCoordinator) {
        
        return;
    }
    
    NSManagedObjectContext * __strong strongContext = context;
    
    [strongContext performBlock: ^{
        
        NSError *saveError;
        BOOL result = [strongContext save:&saveError];
        if (!result) {
            
            if (completion) {
                
                completion(saveError ?: [NSError errorWithDomain:@"TKT"
                                                            code:1002
                                                        userInfo:@{ NSLocalizedDescriptionKey:@"Cannot save local data" }
                                        ]);
            }
        } else {
            
            NSManagedObjectContext *parentContext = strongContext.parentContext;
            if (parentContext) {
                
                [parentContext performBlock:^{
                    
                    [self saveContext:parentContext withCompletion:completion];
                }];
            } else if (completion) {
                
                completion(nil);
            }
        }
    }];
}

@end
