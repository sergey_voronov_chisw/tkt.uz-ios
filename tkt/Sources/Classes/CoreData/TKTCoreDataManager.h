//
//  TKTCoreDataManager.h
//  tkt
//
//  Created by Sergey Voronov on 3/19/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSPersistentContainer, NSManagedObjectContext, NSManagedObject, NSManagedObjectID;

@interface TKTCoreDataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

TKT_SHARED_INSTANCE_DECLARATION

#pragma mark - General

+ (void)cleanDatabase;

#pragma mark General Objects Methods

+ (id)newObjectOfClass:(Class)class;
+ (void)deleteObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context;
+ (void)deleteObjectsOfClass:(Class)class;
+ (NSManagedObject *)existingObjectWithObjectID:(NSManagedObjectID *)managedObjectID inContext:(NSManagedObjectContext *)context;
+ (NSUInteger)countOfObjectsWithClass:(Class)class andPredicate:(NSPredicate *)predicate;
+ (NSArray *)objectsWithClass:(Class)class andPredicate:(NSPredicate *)predicate;
+ (id)calcValue:(NSString *)valueName
       forField:(NSString *)fieldName
        ofClass:(Class)class
   andPredicate:(NSPredicate *)predicate;

#pragma mark General Context Methods

+ (NSManagedObjectContext *)managedObjectContext;

#pragma mark General Context Save Methods

+ (void)save:(NSError **)error;
+ (void)saveWithCompletion:(void (^)(NSError *))completion;

@end
