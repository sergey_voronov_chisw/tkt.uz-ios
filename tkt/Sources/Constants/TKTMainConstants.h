//
//  TKTMainConstants.h
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#ifndef TKTMainConstants_h
#define TKTMainConstants_h

static NSString* const CURRENT_ENVIRONMENT =
#if defined (STAGING)
    @"STAGING";
#else
    @"PRODUCTION";
#endif


#endif /* TKTMainConstants_h */
