//
//  TKTSharedInstanceMacros.h
//  tkt
//
//  Created by Sergey Voronov on 3/19/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import <UIKit/UIKit.h>


#define TKT_SHARED_INSTANCE_DECLARATION    \
    + (instancetype)sharedInstance;


#define TKT_SHARED_INSTANCE_IMPLEMENTATION \
    + (instancetype)sharedInstance {       \
                                           \
        static id sharedInstance;          \
        static dispatch_once_t onceToken;  \
        dispatch_once(&onceToken, ^{       \
                                           \
            sharedInstance = [self new];   \
                                                                                        \
_Pragma ("clang diagnostic push")                                                       \
_Pragma ("clang diagnostic ignored \"-Wundeclared-selector\"")                          \
            if ([sharedInstance respondsToSelector:@selector(initializeInstance)]) {    \
                                                                                        \
                [sharedInstance performSelector:@selector(initializeInstance)];         \
            }                                                                           \
_Pragma ("clang diagnostic pop")                                                        \
                                                                                        \
        });                                \
        return sharedInstance;             \
    }
