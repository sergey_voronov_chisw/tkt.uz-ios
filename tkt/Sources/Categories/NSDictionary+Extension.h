//
//  NSDictionary+Extension.h
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)

- (NSDictionary *)dictionaryForKey:(NSString *)key;

@end
