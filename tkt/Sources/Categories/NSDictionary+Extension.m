//
//  NSDictionary+Extension.m
//  tkt
//
//  Created by Sergey Voronov on 3/20/18.
//  Copyright © 2018 CHI Software. All rights reserved.
//

#import "NSDictionary+Extension.h"

@implementation NSDictionary (Extension)

- (NSDictionary *)dictionaryForKey:(NSString *)key {
    id value = [self valueForKey:key];
    if (value && [value isKindOfClass:[NSDictionary class]]) {
        return value;
    } else {
        return nil;
    }
}

@end
